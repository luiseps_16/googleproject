package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class GoogleTraductorPage {

	public static final Target BOTON_LENGUAJE_ORIGEN = Target.the("El Botón idioma de origen").located(By.xpath("//div[@class='sl-more tlid-open-source-language-list']"));
	public static final Target BOTON_LENGUAJE_DESTINO = Target.the("El Botón idioma de destino").located(By.xpath("//div[@class='tl-more tlid-open-target-language-list']"));
	public static final Target OPCION_ESPANOL = Target.the("La opción español").located(By.xpath("//div[@class='language_list_item_icon tl_list_es_checkmark']"));
	public static final Target OPCION_INGLES = Target.the("La opción inglés").located(By.xpath("//div[@class='language_list_item_icon sl_list_en_checkmark']"));
	public static final Target AREA_DE_TRADUCCION = Target.the("El lugar dónde se escriben las palabras a traducir").located(By.id("source"));
	public static final Target AREA_TRADUCIDA = Target.the("El área donde se presentan los resultados de la traducción").located(By.xpath("//div[@class='tlid-results-container results-container']"));
	
	
}
