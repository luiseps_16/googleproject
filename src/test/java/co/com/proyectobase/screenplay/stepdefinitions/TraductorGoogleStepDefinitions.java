package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefinitions {
	
	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	private Actor luis = Actor.named("Luis;");
	
	@Before
	public void configuracionInicial() {
		luis.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^that Luis want to use Google Translate$")
	public void thatLuisWantToUseGoogleTranslate() throws Exception {
		luis.wasAbleTo(Abrir.LaPaginaDeGoogle());
	}


	@When("^he translate the word (.*) from english to spanish$")
	public void heTranslateTheWordTableFromEnglishToSpanish(String palabra) {
		luis.attemptsTo(Traducir.DeInglesAEspanol(palabra));
	}

	@Then("^he should to see the word (.*) on the screen$")
	public void heShouldToSeeTheWordMesaOnTheScreen(String palabraEsperada) {
		luis.should(seeThat(LaRespuesta.es(), equalTo(palabraEsperada)));
	}

}
